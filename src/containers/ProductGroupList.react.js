import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

import AddButtonWithText from '../components/AddButtonWithText.react'
import BottomBar from '../components/BottomBar.react'
import Modal from '../components/Modal.react'
import ProductGroupItem from '../components/ProductGroupItem.react'
import StickyBox from '../components/StickyBox.react'
import { getProductGroupShop, deleteProductGroup } from '../rest'

import './ProductGroupList.css'

const ProductGroupList = (props) => {
  const [productGroups, setProductGroups] = useState([])
  const user = useSelector((state) => state.user)

  const [deleteProductGroupModalVisible, setDeleteProductGroupModalVisible] = useState(false)
  const [selectedProductGroupId, setSelectedProductGroupId] = useState(
    props.location.state && props.location.state.groupId,
  )

  useEffect(() => {
    const fetchProductGroups = async () => {
      const response = await getProductGroupShop(user.chattyShopId)
      setProductGroups(response.product_group_data)
    }
    fetchProductGroups()
  }, [user.chattyShopId])

  const onConfirmDelete = async () => {
    try {
      await deleteProductGroup(selectedProductGroupId)
      setDeleteProductGroupModalVisible(false)

      const response = await getProductGroupShop(user.chattyShopId)
      setProductGroups(response)
    } catch (error) {
      console.error('Error:onConfirmDelete: ', error)
    }
  }

  const onProductGroupSelected = (productGroupId) => {
    setSelectedProductGroupId(productGroupId)
  }

  const onSelectToDelete = (productGroupId) => {
    setSelectedProductGroupId(productGroupId)
    setDeleteProductGroupModalVisible(true)
  }

  const onCancelSelectToDelete = () => {
    setSelectedProductGroupId(null)
    setDeleteProductGroupModalVisible(false)
  }

  const renderDeleteProductGroupModal = () => {
    if (!deleteProductGroupModalVisible) return null

    return (
      <Modal
        description="คุณต้องการลบกลุ่มสินค้านี้หรือไม่"
        leftActionText="ยกเลิก"
        rightActionText="ตกลง"
        onCancel={onCancelSelectToDelete}
        onConfirm={onConfirmDelete}
      />
    )
  }

  const renderProductGroupItems = () => {
    return productGroups.map((item, index) => (
      <ProductGroupItem
        key={index}
        productGroup={item}
        checked={item.product_group_id === selectedProductGroupId}
        selectionEnable={!!selectedProductGroupId}
        onDelete={onSelectToDelete}
        onSelect={onProductGroupSelected}
      />
    ))
  }

  const renderBottomBar = () => {
    if (!selectedProductGroupId) return null

    return (
      <BottomBar
        buttonText="ยินยันการเลือกสินค้า"
        onSubmit={() => null}
        onBack={() => props.history.goBack()}
      />
    )
  }

  return (
    <div className="product-group">
      <StickyBox title="กลุ่มสินค้า" />
      <div className="product-group__pane">
        <AddButtonWithText url="productGroup" text="เพิ่มกลุ่มสินค้า" />
        {renderProductGroupItems()}
      </div>
      {renderDeleteProductGroupModal()}
      {renderBottomBar()}
    </div>
  )
}

export default ProductGroupList
