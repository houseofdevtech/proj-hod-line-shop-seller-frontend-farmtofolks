import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import _ from 'lodash'
import { Input, Divider } from 'semantic-ui-react'

import BottomBar from '../components/BottomBar.react'
import ImageUploader from '../components/ImageUploader.react'
import ProductBox from '../components/ProductBox.react'
import SelectableCardList from '../components/SelectableCardList.react'
import StickyBox from '../components/StickyBox.react'
import { createProductGroup, updateProductGroup, getProducts } from '../rest'

import './ProductGroup.css'

const ProductGroup = (props) => {
  const [productGroupName, setProductGroupName] = useState(
    props.history.location.state ? props.history.location.state.product_group.name : '',
  )
  const [image, setImage] = useState([
    {
      url: props.history.location.state
        ? props.history.location.state.product_group.main_img_path
        : '',
      isCoverImage: true,
    },
  ])
  const [products, setProducts] = useState([])
  const [selectedItems, setSelectedItems] = useState([])
  const user = useSelector((state) => state.user)

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await getProducts(user.chattyShopId, 'all')
      setProducts(response.product_data)
    }
    fetchProducts()
  }, [user.chattyShopId])

  const onGetSelectedItems = (items) => {
    setSelectedItems(items)
  }

  const onInputChange = (e) => setProductGroupName(e.target.value)

  const onProductGroupCreateUpdate = async () => {
    if (props.history.location.state) {
      try {
        await updateProductGroup(user.chattyShopId, {
          body_data: {
            products_group_id: '',
            name: productGroupName,
            items: [],
          },
          main_img_path: '',
        })

        props.history.goBack()
      } catch (error) {
        console.error('Error:updateProductGroup', error)
      }
    } else {
      try {
        await createProductGroup({
          // mock
          body_data: {
            name: productGroupName,
            items: [],
            path_gcs_name: '',
          },
          main_img_path: image.url,
        })

        props.history.goBack()
      } catch (error) {
        console.error('Error:createProductGroup', error)
      }
    }
  }

  // TODO: Replace with props route somehow
  const renderProductBoxes = () => {
    if (_.isEmpty(products)) {
      return []
    }

    return products.map((product, index) => <ProductBox product={product} count={index + 1} />)
  }

  return (
    <div className="product-group">
      <StickyBox title="ตั้งค่ากลุ่มสินค้า" backButtonDisable />
      <div className="product-group__container">
        <div className="product-group__title">กรุณาเลือกสินค้าเพื่อตั้งค่ากลุ่มสินค้า</div>
        <div className="product-group__input">
          <Input
            name="productGroupName"
            size="small"
            placeholder="ชื่อกลุ่มสินค้า"
            value={productGroupName}
            onChange={onInputChange}
          />
        </div>
        <div className="product-group__uploader">
          <ImageUploader
            images={image}
            onImageUploadSelect={(data) => setImage(data)}
            showOneItem
          />
        </div>
        <div className="product-group__divider">
          <Divider />
          <div className="product-group__divider__text">
            * กลุ่มสินค้าสามารถตั้งได้แค่ 4 กลุ่มเท่านั้น
          </div>
        </div>
        <div className="product-group__items">
          {!_.isEmpty(products) ? (
            <SelectableCardList
              contents={renderProductBoxes()}
              maxSelectable={10}
              onGetSelectedItems={onGetSelectedItems}
              multiple
              countEnable
            />
          ) : (
            'No products'
          )}
        </div>
      </div>
      <BottomBar
        buttonText="ยินยันการเลือกสินค้า"
        onBack={() => props.history.goBack()}
        onSubmit={onProductGroupCreateUpdate}
        buttonDisable={productGroupName.trim.length === 0 && _.isEmpty(selectedItems)}
      />
    </div>
  )
}

export default ProductGroup
