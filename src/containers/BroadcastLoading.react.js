import React from 'react'

import './BroadcastLoading.css'

const BroadcastLoading = (props) => {
  return (
    <div className="broadcast-loading">
      <div className="broadcast-loading__box">
        <span>{false ? 'กำลังทำการบรอดแคสต์...' : 'ยินดีด้วย การบรอดแคสต์สำเร็จ!'}</span>
      </div>
    </div>
  )
}

export default BroadcastLoading
