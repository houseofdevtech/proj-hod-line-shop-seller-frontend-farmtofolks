import React, { useState, useEffect } from 'react'
import _ from 'lodash'
import { Input, Button } from 'semantic-ui-react'

import CalendarModalShow from '../components/CalendarModal.react'
import ClockModalShow from '../components/ClockModal.react'
import BackArrow from '../img/back_arrow.png'

import './FilterOrders.css'

const FilterOrders = (props) => {
  const [startDate, setStartDate] = useState('')
  const [startTime, setStartTime] = useState('00:00')
  const [endDate, setEndDate] = useState('')
  const [endTime, setEndTime] = useState('00:00')
  const [showClockStart, setShowClockStart] = useState(false)
  const [showClockEnd, setShowClockEnd] = useState(false)
  const [showCalendarStart, setShowCalendarStart] = useState(false)
  const [showCalendarEnd, setShowCalendarEnd] = useState(false)

  // const onStartDateSet = (e) => {
  //   if (e.target.value < endDate && endDate !== '') {
  //     setEndDate('')
  //   }
  //   setStartDate(e.target.value)
  // }

  useEffect(() => {
    const newStartTime = startTime.replace(':', '.')
    const newEndTime = endTime.replace(':', '.')
    console.log(`${startDate} : ${newStartTime}  || ${endDate} : ${newEndTime} `)
    // return () => {
    // }
  }, [startDate, startTime, endDate, endTime])

  const time = (type) => {
    return type === 'start' && startTime !== ''
      ? startTime
      : type === 'end' && endTime !== ''
      ? endTime
      : null
  }

  return (
    <div className="filter-orders">
      <div className="filter-orders__box-selection">
        <div className="filter-orders__box-selection__title">
          เลือกวันที่คุณต้องการดูสรุปรายงานคำสั่งซื้อ
        </div>
        <div className="filter-orders__box-selection__start-from">
          <div>ตั้งแต่</div>
          <Input
            name="startDate"
            placeholder="วันที่เริ่มต้น"
            readOnly="readonly"
            value={startDate}
            onChange={(e) => setStartDate(e.target.value)}
            onClick={() => setShowCalendarStart(true)}
          />
          <Input
            name="startTime"
            placeholder="เวลาเริ่มต้น"
            readOnly="readonly"
            value={startTime}
            onChange={(e) => setStartTime(e.target.value)}
            onClick={() => setShowClockStart(true)}
          />
        </div>
        <div className="filter-orders__box-selection__end-date">
          <div>ถึง</div>
          <Input
            name="endDate"
            placeholder="วันที่สิ้นสุด"
            readOnly="readonly"
            value={endDate}
            min={startDate}
            onChange={(e) => setEndDate(e.target.value)}
            onClick={() => setShowCalendarEnd(true)}
          />
          <Input
            name="endTime"
            placeholder="เวลาสุดท้าย"
            readOnly="readonly"
            value={endTime}
            onChange={(e) => setEndTime(e.target.value)}
            onClick={() => setShowClockEnd(true)}
          />
        </div>
        <div className="filter-orders__box-selection__actions">
          <div
            className="filter-orders__box-selection__actions__back-button"
            onClick={() => props.history.goBack()}
          >
            <img src={BackArrow} alt="back-button" />
          </div>

          <Button
            className="filter-orders__box-selection__actions__button"
            onClick={() => {
              const newStartTime = startTime.replace(':', '.')
              const newEndTime = endTime.replace(':', '.')
              props.history.push(
                `orderlist/?startDate=${startDate}&startTime=${newStartTime}&endDate=${endDate}&endTime=${newEndTime}`,
              )
            }}
            disabled={_.includes([startDate, startTime, endTime, endDate], '')}
          >
            บันทึก
          </Button>
        </div>
      </div>

      <ClockModalShow
        isOpen={showClockStart || showClockEnd}
        onChange={showClockStart ? setStartTime : showClockEnd ? setEndTime : null}
        time={showClockStart ? time('start') : showClockEnd ? time('end') : null}
        closeModal={() => {
          if (showClockStart) {
            setShowClockStart(false)
          }
          if (showClockEnd) {
            setShowClockEnd(false)
          }
        }}
      />
      <CalendarModalShow
        isOpen={showCalendarStart || showCalendarEnd}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
        closeModal={() => {
          if (showCalendarStart) {
            setShowCalendarStart(false)
          }
          if (showCalendarEnd) {
            setShowCalendarEnd(false)
          }
        }}
      />
    </div>
  )
}

export default FilterOrders
