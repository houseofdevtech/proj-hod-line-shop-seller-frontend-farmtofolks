import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import queryString from 'query-string'

import request from '../api'
import OrderCard from '../components/OrderCard.react'
import StickyBox from '../components/StickyBox.react'

import './OrderList.css'

// import { MOCK_ORDERS } from '../mocks'
// 190114 overall status from backend
// 0 waitslip   "ยังไม่ได้ชำระสินค้า"
// 1 packing    "รอบรรจุ"/ "ชำระเงินแล้ว"
// 2 delivery   "รอจัดส่ง"
// 3 complete  "แจ้งเลขพัสดุแล้ว"
// 4 cancel     "ยกเลิกรายการ"

//<OrderList handleChange={handleChange} data={data} />

const OrderList = (props) => {
  const [filterBy, setFilterBy] = useState('all')
  const [queryStr, setQueryStr] = useState(queryString.parse(props.location.search))
  const [data, setData] = useState(null)
  const user = useSelector((state) => state.user)

  const onFilterSet = (filter) => {
    setFilterBy(filter)
    if (filter === 'all') {
      setQueryStr({})
      props.history.push('/orderlist')
    }
  }

  useEffect(() => {
    const shopId = user.chattyShopId
    const getTrackingAll = async () => {
      if (shopId !== '') {
        try {
          const { data } = await request.get(`/api/tracking/shop/${shopId}?type=all&history=false`)
          if (data) {
            console.log(data)
            const formattedData = formatData(data[0].tracking_items)
            setQueryStr('')
            setData(formattedData)
          }
        } catch (error) {
          console.log(error)
        }
      }
    }
    const getTrackingHistory = async (obj) => {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })
      if (shopId !== '') {
        try {
          const { data } = await request.get(
            `/api/tracking/shop/${shopId}?from_d=${obj.startDate}&from_h=${obj.startTime}&to_d=${obj.endDate}&to_h=${obj.endTime}&type=all&history=true`,
          )
          if (data) {
            const formattedData = formatData(data[0].tracking_items)
            setData(formattedData)
          }
        } catch (error) {
          console.log(error)
        }
      }
    }
    console.log(`queryStr :: ${JSON.stringify(queryStr)}`)
    if (_.isEmpty(queryStr)) {
      console.log('all')
      getTrackingAll()
    } else {
      console.log('queryString')
      getTrackingHistory(queryStr)
    }
  }, [filterBy, queryStr, user.chattyShopId])

  const formatData = (inputData) => {
    // add Data Type
    if (inputData) {
      const result = inputData.map((obj) => {
        if (obj.status === 'ยังไม่ได้ชำระสินค้า') {
          return { ...obj, type: 'waitslip' }
        }
        if (obj.status === 'รอบรรจุ' || obj.status === 'ชำระเงินแล้ว') {
          return { ...obj, type: 'packing' }
        }
        if (obj.status === 'รอจัดส่ง') {
          return { ...obj, type: 'delivery' }
        }
        if (obj.status === 'แจ้งเลขพัสดุแล้ว') {
          return { ...obj, type: 'complete' }
        }
        if (obj.status === 'ยกเลิกรายการ') {
          return { ...obj, type: 'cancel' }
        }
        return null
      })
      console.log(result)
      return result
    }
  }

  const renderOrderCards = () => {
    // if data is loaded (from useEffect())
    if (data !== null) {
      if (filterBy === 'all') {
        return _.chain(data)
          .sortBy((item) => item.type)
          .reverse()
          .map((item, index) => {
            // if data_order exist in props.item.tracking_items
            // then render order card
            if ('data_order' in item.tracking_items) {
              return <OrderCard key={index} type={item.type} item={item} />
            }
            return null
          })
          .value()
      }
      const filtered = _.filter(data, (order) => order.type === filterBy)
      return _.map(filtered, (item, index) => {
        if ('data_order' in item.tracking_items) {
          return <OrderCard key={index} type={item.type} item={item} />
        }
      })
    }
  }

  // const renderOrderCards = () => {
  //   if (filterBy === 'all') {
  //     return _.chain(MOCK_ORDERS)
  //       .sortBy((item) => item.type)
  //       .reverse()
  //       .map((item, index) => <OrderCard key={index} type={item.type} />)
  //       .value()
  //   }
  //   const filtered = _.filter(MOCK_ORDERS, (order) => order.type === filterBy)
  //   return _.map(filtered, (item, index) => <OrderCard key={index} type={item.type} />)
  // }

  const renderFilteredData = () => {
    if (_.isEmpty(queryStr)) return
    return (
      <div className="order-list__list-container__filtered">
        <div className="order-list__list-container__filtered__title">สรุปรายการคำสั่งซื้อ</div>
        <div>
          ตั้งแต่วันที่ {queryStr.startDate}: {queryStr.startTime} น.
          <br /> &emsp; ถึง &nbsp;
          {queryStr.endDate} : {queryStr.endTime} น.
        </div>
        <div>ยอดขายรวม xxxx ชิ้น ราคารวม xxxx บาท</div>
      </div>
    )
  }

  return (
    <div className="order-list">
      <StickyBox title="จัดการคำสั่งซื้อและการจัดส่ง" />
      <div className="order-list__container">
        <div className="order-list__title">รายการคำสั่งซื้อ</div>
        <div className="order-list__tabs">
          <div className="order-list__lists">
            <ul>
              <li
                onClick={() => onFilterSet('all')}
                style={{
                  textDecoration: filterBy === 'all' ? 'underline' : 'none',
                  fontWeight: filterBy === 'all' ? '600' : undefined,
                }}
              >
                ทั้งหมด
              </li>
              <li
                onClick={() => onFilterSet('waitslip')}
                style={{
                  textDecoration: filterBy === 'waitslip' ? 'underline' : 'none',
                  fontWeight: filterBy === 'waitslip' ? '600' : undefined,
                  borderLeft: '2px solid #e75336',
                  paddingLeft: '2px',
                }}
              >
                รอยืนยันการชำระเงิน
              </li>
              <li
                onClick={() => onFilterSet('packing')}
                style={{
                  textDecoration: filterBy === 'packing' ? 'underline' : 'none',
                  fontWeight: filterBy === 'packing' ? '600' : undefined,
                  borderLeft: '2px solid #6565f7',
                  paddingLeft: '2px',
                }}
              >
                รอบรรจุ
              </li>
              <li
                onClick={() => onFilterSet('delivery')}
                style={{
                  textDecoration: filterBy === 'delivery' ? 'underline' : 'none',
                  fontWeight: filterBy === 'delivery' ? '600' : undefined,
                  borderLeft: '2px solid #f7d865',
                  paddingLeft: '2px',
                }}
              >
                รอจัดส่ง
              </li>
              <li
                onClick={() => onFilterSet('complete')}
                style={{
                  textDecoration: filterBy === 'complete' ? 'underline' : 'none',
                  fontWeight: filterBy === 'complete' ? '600' : undefined,
                  borderLeft: '2px solid #55a18d',
                  paddingLeft: '2px',
                }}
              >
                แจ้งเลขพัสดุแล้ว
              </li>
              <li
                onClick={() => onFilterSet('cancel')}
                style={{
                  textDecoration: filterBy === 'cancel' ? 'underline' : 'none',
                  fontWeight: filterBy === 'cancel' ? '600' : undefined,
                  borderLeft: '2px solid #000000',
                  paddingLeft: '2px',
                }}
              >
                ยกเลิกรายการ
              </li>
            </ul>
          </div>
        </div>
        <div className="order-list__list-container">
          <div className="order-list__list-subcontainer">
            {renderFilteredData()}
            <div className="order-list__list-container__summary">
              <Link to="/filterOrders">
                {_.isEmpty(queryStr) ? 'ดูสรุปรายการคำสั่งซื้อ' : 'แก้ไขวันที่สรุปรายการคำสั่งซื้อ'}
              </Link>
            </div>

            {renderOrderCards()}
          </div>
        </div>
      </div>
    </div>
  )
}

export default OrderList
