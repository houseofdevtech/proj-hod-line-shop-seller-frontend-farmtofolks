let picBaseURL = null

if (process.env.REACT_APP_DEV === 'local') {
  // picBaseURL = 'https://uat.chatty.shop/backend/'
  picBaseURL = 'https://chattyshop.houseofdev.tech/backend'
}
if (process.env.REACT_APP_DEV === 'dev') {
  picBaseURL = 'https://chattyshop.houseofdev.tech/backend/'
}
if (process.env.REACT_APP_DEV === 'uat') {
  picBaseURL = 'https://uat.chatty.shop/backend/'
}
if (process.env.REACT_APP_DEV === 'passione') {
  picBaseURL = 'https://apichattyshop.passione.co.th/'
}

export default picBaseURL
