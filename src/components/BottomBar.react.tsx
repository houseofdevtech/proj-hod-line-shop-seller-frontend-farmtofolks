import React from 'react'
import classNames from 'classnames'

import './BottomBar.css'

type Props = {
  buttonText: string
  buttonDisable?: boolean
  submitting?: boolean
  onBack: () => void
  onSubmit: () => void
}

const BottomBar = (props: Props) => {
  return (
    <div className="bottom-bar">
      <div className="bottom-bar__back-button" onClick={props.onBack}>
        <img src={require('../img/back_arrow.png')} alt="back-button" />
      </div>
      <button
        className={classNames('bottom-bar__button', {
          '--disabled': props.buttonDisable || props.submitting,
        })}
        disabled={props.buttonDisable}
        onClick={props.onSubmit}
      >
        {props.submitting ? 'กรุณารอซักครู่...' : props.buttonText}
      </button>
    </div>
  )
}

export default BottomBar
