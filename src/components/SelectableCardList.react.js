import React from 'react'
import PropTypes from 'prop-types'

import SelectableTitleCard from './SelectableTitleCard.react'

class SelectableCardList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      item: null,
      index: this.props.multiple
        ? []
        : this.props.contents.findIndex((item) => item.value === this.props.selectedValue) === 0 // Notes: cannot set default when value equal `0` e.g. 0 || 1 ==  `1` not `0`
        ? '0'
        : this.props.contents.findIndex((item) => item.value === this.props.selectedValue) || -1,
      mapIndexCount: {}, // Notes: { [index]: count } => e.g { 0: 2 } [index 0 has count on position 2]
    }
    this.onItemSelected = this.onItemSelected.bind(this)
  }

  componentDidMount() {
    const currentSelected = this.props.currentSelected || []
    currentSelected.map((item, index) => {
      this.onItemSelected(item, index)
    })
  }

  onItemSelected(item, index) {
    // Notes: check mode that multiple select and single select
    if (this.props.multiple) {
      this.setState((prevState, props) => {
        const selectedIndexes = prevState.index
        const selectedIndex = selectedIndexes.indexOf(index)
        let finalMapIndexCount

        // Notes: deselect case
        if (selectedIndex > -1) {
          selectedIndexes.splice(selectedIndex, 1)
          const currentCount = prevState.mapIndexCount[index]
          // Notes: check current index value(count) is greater than the other or not
          // if true, minus one the rest of index
          for (const index in prevState.mapIndexCount) {
            const eachCount = prevState.mapIndexCount[index]
            if (eachCount > currentCount) {
              prevState.mapIndexCount[index]--
            }
          }
          finalMapIndexCount = prevState.mapIndexCount
        } else {
          if (!(selectedIndexes.length >= props.maxSelectable)) {
            selectedIndexes.push(index)

            finalMapIndexCount = {
              ...prevState.mapIndexCount,
              [index]: prevState.index.length,
            }
          }
        }
        const contents = this.props.contents.map((product) => {
          if (typeof product.value === 'object') {
            return product.props.product
          }
          return product
        })

        const selectedItems = this.state.index.map((i) => contents[i])
        this.props.onGetSelectedItems(selectedItems, item, index)

        return {
          index: selectedIndexes,
          mapIndexCount: finalMapIndexCount,
        }
      })
    } else {
      this.setState({ item, index })
      if (this.props.onChange) {
        this.props.onChange(item, index)
      }
    }
  }

  render() {
    const content = this.props.contents.map((item, index) => {
      const selected = this.props.multiple
        ? this.state.index.indexOf(index) > -1
        : this.state.index == index //  Notes: uses `==` for handle case index equal `0`

      return (
        <SelectableTitleCard
          key={index}
          item={item}
          selected={selected}
          count={this.state.mapIndexCount[index]}
          countEnable={this.props.countEnable}
          enableRemoveItem={this.props.enableRemoveItem}
          onClick={() => {
            if (this.props.disabled) {
              return
            }
            this.onItemSelected(item, index)
          }}
          onItemDelete={this.props.onItemDelete}
        />
      )
    })
    return content
  }
}

SelectableCardList.propTypes = {
  contents: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  selectedValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  maxSelectable: PropTypes.number,
  multiple: PropTypes.bool,
  countEnable: PropTypes.bool,
  enableRemoveItem: PropTypes.bool,
  onChange: PropTypes.func,
  onItemDelete: PropTypes.func,
  onGetSelectedItems: PropTypes.func,
}

export default SelectableCardList
