import React from 'react'
import classNames from 'classnames'

import './SelectableCard.css'

const SelectableCard = (props) => {
  const className = classNames('selectable-card', {
    '--selected': props.item.selected,
  })

  const isSelected = props.enableRemoveItem ? '' : props.selected ? 'selected' : ''
  const subClassName = 'selectable ' + isSelected

  const style = props.item.color_img_path
    ? {
        backgroundImage: `url(${props.item.color_img_path})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
      }
    : { background: props.item.color_value }

  return (
    <div className={className} style={style}>
      <div className={subClassName} onClick={props.onClick}>
        {props.children}
        {props.countEnable && <div className="check">{props.count}</div>}
      </div>
    </div>
  )
}

export default React.memo(SelectableCard)
