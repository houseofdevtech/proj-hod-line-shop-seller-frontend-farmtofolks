import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import moment from 'moment'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import picBaseURL from '../apiPicUrl'
// import { picBaseURL } from '../api'
// import SampleImage from '../img/wall.jpg'
import Woman1 from '../img/woman1.png'

import './OrderCard.css'

const OrderCard = (props) => {
  const [expandVisible, setExpandVisible] = useState(false)
  const className = classNames('order-card', {
    '--waitslip': props.type === 'waitslip',
    '--packing': props.type === 'packing',
    '--delivery': props.type === 'delivery',
    '--cancel': props.type === 'cancel',
    '--complete': props.type === 'complete',
    // "--notified": props.type === "notified",
  })

  const { date_order, tracking_number, slip_img_path } = props.item
  const {
    bill_code,
    total,
    data_order,
    sum_qty,
    sum_price,
    tracking_price,
    shipping_type,
  } = props.item.tracking_items
  let { pos } = props.item
  if (pos === null || pos === '') {
    pos = '-'
  }
  const data_user_name = props.item.tracking_items.data_user
    ? props.item.tracking_items.data_user.name
    : undefined
  const name = data_user_name ? data_user_name : 'ไม่มีชื่อ'

  const date = moment.utc(date_order).locale('th')
  const formattedOrderDate = moment(date)
    .local()
    .format('DD/MM/YY : HH.mm น.')
  const totalPrice = total
  // const isOrder = 'data_order' in props.item.tracking_items
  // console.log( `data_order :: ${isOrder}`)
  // console.log(props.item)
  return (
    <div className={className}>
      <div className="order-card__information">
        <div className="order-card__information__label">
          <div>Bill Code :</div>
          <div>ชื่อ นามสกุล :</div>
          <div>วันที่สั่งซื้อ :</div>
          <div>ยอดสั่งซื้อ :</div>
          {pos !== null && <div>รหัสบิลอ้างอิง :</div>}
        </div>
        <div className="order-card__information__data">
          {/* <div>AB1234567890</div> */}
          {/* <div>นางสาวสวย ช้อปปิ้งเก่ง</div> */}
          {/* <div>12/09/19 : 12.05 น.</div> */}
          <div>{bill_code}</div>
          <div>{name}</div>
          <div>{formattedOrderDate}</div>
          <div>{totalPrice.toLocaleString('th')} THB</div>
          <div>{pos}</div>
        </div>
      </div>
      <div className="order-card__actions">
        <div className="order-card__actions__button">
          <Link
            to={{
              pathname: `/updateOrder/${props.item.tracking_id}`,
              state: {
                trackingId: props.item.tracking_id,
                itemData: props.item,
                orderStatus: props.type, // TODO: Pass real order data
              },
            }}
          >
            <Button>
              {props.type === 'complete' ? (
                <div className="order-card__actions__button__text">
                  <div>แจ้งเลขพัสดุแล้ว</div>
                  <div>{tracking_number !== null ? tracking_number : 'null จร้า'}</div>
                </div>
              ) : props.type === 'packing' ? (
                'รอบรรจุ'
              ) : props.type === 'waitslip' ? (
                'รอยืนยันชำระเงิน'
              ) : props.type === 'cancel' ? (
                'ยกเลิกรายการ'
              ) : (
                'รอจัดส่ง'
              )}
            </Button>
          </Link>
        </div>
        <div className="order-card__actions__more" onClick={() => setExpandVisible(!expandVisible)}>
          ดูรายละเอียดสินค้า
          <div
            className={classNames('order-card__actions__more__dropdown-icon', {
              '--arrow-up': expandVisible,
            })}
          ></div>
        </div>
      </div>
      {expandVisible && (
        <div className="order-card__detail">
          {slip_img_path !== null && (
            <div className="order-card__detail slip">
              <img src={slip_img_path} alt="slip-img" />
              {/* <img src={SampleImage} alt="slip-img" /> */}
              <div>สลิปยืนยันการโอนเงิน</div>
            </div>
          )}
          <div className="order-card__detail__summary__divider" />
          {data_order.order_items.map((order_item) => {
            //console.log(order_item)
            return order_item.product_items.map((obj) => {
              //console.log(obj)
              return (
                <div className="order-card__detail__item">
                  {obj.color.color_img_path !== null && (
                    <img style={{ width: 100 }} src={obj.color.color_img_path} alt="shopItem" />
                  )}
                  <div className="order-card__detail__item__label-price">
                    <div className="order-card__detail__item__label-price__label">
                      <div>{order_item.product_name}</div>
                      <div>Barcord: {order_item.product_items[0].sku_code}</div>
                      <div>ขนาด : {obj.size}</div>
                      <div>
                        <div> สี : {obj.color.color_name} </div>
                      </div>
                      <div>SKU code : {obj.sku_code}</div>
                    </div>
                    <div className="order-card__detail__item__label-price__price">
                      <div>x {obj.qty}</div>
                      <div>&nbsp;</div>
                      <div>{obj.sum.toLocaleString('th')} THB</div>
                    </div>
                  </div>
                </div>
              )
            })
          })}

          <div className="order-card__detail__summary">
            <div className="order-card__detail__summary__item">
              <div>รวม x {sum_qty}</div>
              <div>{sum_price.toLocaleString('th')} THB</div>
            </div>
            <div className="order-card__detail__summary__item">
              <div>{shipping_type}</div>
              <div>{tracking_price.toLocaleString('th')} THB</div>
            </div>
            <div className="order-card__detail__summary__divider" />
            <div className="order-card__detail__summary__item">
              <div>รวมทั้งหมด</div>
              <div>{totalPrice.toLocaleString('th')} THB</div>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

OrderCard.propTypes = {
  type: PropTypes.string,
}

export default OrderCard
