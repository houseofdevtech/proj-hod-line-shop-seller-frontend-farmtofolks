import React from 'react'
import { withRouter } from 'react-router-dom'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import './StickyBox.css'

const StickyBox = (props) => {
  const onBack = () => {
    props.history.goBack()
  }

  return (
    <div className="sticky-box">
      <div
        className={classNames('sticky-box__back', {
          '--hide': props.backButtonDisable,
        })}
        onClick={onBack}
      >
        &#60;
      </div>
      <div className="sticky-box__title">{props.title}</div>
      <div></div>
    </div>
  )
}

StickyBox.propTypes = {
  title: PropTypes.string.isRequired,
  backButtonDisable: PropTypes.bool,
}

export default withRouter(StickyBox)
