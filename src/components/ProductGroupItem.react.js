import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import Trash from '../img/trash.png'
import ProductBox from './ProductBox.react.js'

import './ProductGroupItem.css'

const ProductGroupItem = (props) => {
  const [expandVisible, setExpandVisible] = useState(false)

  const onProductSelected = () => {
    props.onSelect(props.productGroup.product_group_id)
  }

  const onProductGroupSelectToDelete = () => {
    props.onDelete(props.productGroup.product_group_id)
  }

  const renderProducts = () => {
    return props.productGroup.product_group_items.map((product, index) => (
      <ProductBox key={index} product={product} disableEdit />
    ))
  }

  const renderBottomSection = () => {
    if (!expandVisible) return
    return (
      <div className="product-group-item__bottom">
        <div className="product-group-item__bottom__products">{renderProducts()}</div>
        <div className="product-group-item__bottom__actions">
          <span
            className="product-group-item__bottom__actions__delete"
            onClick={onProductGroupSelectToDelete}
          >
            <img src={Trash} alt="trash" />
          </span>
          <div
            className="product-group-item__bottom__actions__edit"
            onClick={() =>
              props.history.push({
                pathname: `productGroup/${props.productGroup.product_group_id}`,
                state: { product_group: props.productGroup },
              })
            }
          >
            แก้ไขกลุ่มสินค้า
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className="product-group-item">
      <div
        className={classNames('product-group-item__top', {
          '--none-border-radius': expandVisible,
        })}
      >
        {props.selectionEnable && (
          <div className="product-group-item__top__left">
            <input
              type="radio"
              name="productGroup"
              checked={props.checked}
              onChange={onProductSelected}
            />
          </div>
        )}
        <div className="product-group-item__top__center">
          <span>กลุ่มสินค้า: {props.productGroup.product_group_id} &nbsp;</span>
          <span>{props.productGroup.name}</span>
        </div>
        <div className="product-group-item__top__right">
          <div
            className={classNames('product-group-item__top__right__dropdown-icon', {
              '--arrow-up': expandVisible,
            })}
            onClick={() => setExpandVisible(!expandVisible)}
          ></div>
        </div>
      </div>
      {renderBottomSection()}
    </div>
  )
}

ProductGroupItem.propTypes = {
  productGroup: PropTypes.object.isRequired,
  checked: PropTypes.bool,
  selectionEnable: PropTypes.bool,
  onDelete: PropTypes.func.isRequired,
  onSelect: PropTypes.func,
}

export default withRouter(ProductGroupItem)
