import React, { useState } from 'react'
import { SketchPicker, SliderPicker } from 'react-color'
import { Modal, Button, Icon } from 'semantic-ui-react'

import './ColorPicker.css'

const ColorPicker = (props) => {
  const [modalVisible, setModalVisible] = useState(false)
  const [color, setColor] = useState(props.hexColors || '')

  const handleChangeComplete = (selectedColor) => {
    setColor(selectedColor.hex)
    props.onColorSelect(selectedColor.hex, props.color)
  }

  const toggleModal = () => setModalVisible(!modalVisible)
  return (
    <React.Fragment>
      <div className="color-picker" onClick={toggleModal}>
        {!color ? (
          <span className="color-picker__add">+</span>
        ) : (
          <div className="color-picker__selected" style={{ background: color }}></div>
        )}
        <div className="color-picker__title">{props.color}</div>
      </div>

      <Modal open={modalVisible} onClose={toggleModal}>
        <Modal.Content>
          <SketchPicker color={color} onChangeComplete={handleChangeComplete} />
          <SliderPicker color={color} onChangeComplete={handleChangeComplete} />
        </Modal.Content>
        <Modal.Actions>
          <Button basic onClick={toggleModal}>
            <Icon name="remove" /> ปิด
          </Button>
        </Modal.Actions>
      </Modal>
    </React.Fragment>
  )
}
export default ColorPicker
