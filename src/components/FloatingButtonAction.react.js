import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'

import OutSideDetection from './OutsideDetection.react.js'

import './FloatingButtonAction.css'

const FloatingButtonAction = () => {
  const [actionOpen, setActionOpen] = useState(false)

  const onToggleAction = () => setActionOpen(!actionOpen)
  const itemClassNames = classNames('floating-button-action__other__circle', {
    '--rotate': actionOpen,
  })
  return (
    <OutSideDetection onClickOutSide={() => setActionOpen(false)}>
      <div className="floating-button-action">
        <div
          className={classNames('floating-button-action__circle', {
            '--rotate': actionOpen,
          })}
        >
          <span onClick={onToggleAction}>+</span>
        </div>

        <div className="floating-button-action__other">
          <div
            className={classNames('floating-button-action__blur-bg', {
              '--rotate': actionOpen,
            })}
          ></div>
          <div className={itemClassNames}>
            <Link to="/productGroupList">เพิ่มกลุ่มสินค้า</Link>
          </div>
          <div className={itemClassNames}>
            <Link to="/addProduct">เพิ่มสินค้า</Link>
          </div>
          <div className={itemClassNames}>สินค้าติดดาว</div>
        </div>
      </div>
    </OutSideDetection>
  )
}

export default FloatingButtonAction
