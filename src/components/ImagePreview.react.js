import React from 'react'
import PropTypes from 'prop-types'

import './ImagePreview.css'

const ImagePreview = (props) => {
  return (
    <div className="image-preview" onClick={props.onPreviewImageClick}>
      <div className="image-preview__image">
        <img src={props.item.url || require('../img/noneSelectProduct.png')} alt="preview" />
      </div>
      <div className="image-preview__text">
        {props.item.isCoverImage ? 'ภาพปก' : `ภาพที่ ${props.count}`}
      </div>
    </div>
  )
}

export default ImagePreview

ImagePreview.propTypes = {
  item: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onPreviewImageClick: PropTypes.func.isRequired,
}
