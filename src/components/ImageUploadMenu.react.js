import React from 'react'
import classNames from 'classnames'
import { Button } from 'semantic-ui-react'

import OutSideDetection from './OutsideDetection.react'

import './ImageUploadMenu.css'

const ImageUploadMenu = (props) => {
  const renderTest = () => {
    if (!props.position) return
    return <Button onClick={props.onCoverImageSet}>เลือกภาพนี้เป็นภาพปก</Button>
  }
  return (
    <OutSideDetection onClickOutSide={props.onUploadMenuClose}>
      <div
        className={classNames('image-uploader__upload-menu', {
          '--slide': props.openUploadMenu,
        })}
      >
        <div className="image-uploader__blur-bg">
          {renderTest()}
          <Button onClick={props.onImageUpload}>อัพโหลดภาพจากเครื่อง</Button>
          <Button>ถ่ายภาพใหม่</Button>
          <Button>อัพโหลดภาพจาก IG, Facebook</Button>
        </div>
      </div>
    </OutSideDetection>
  )
}

export default ImageUploadMenu
