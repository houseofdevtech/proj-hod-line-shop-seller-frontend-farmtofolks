import React, { useRef, useEffect } from 'react'
import PropTypes from 'prop-types'

function useOutside(ref, props) {
  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      props.onClickOutSide()
    }
  }

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  })
}

const OutsideDetection = (props) => {
  const wrapperRef = useRef(null)
  useOutside(wrapperRef, props)

  return <div ref={wrapperRef}>{props.children}</div>
}

OutsideDetection.propTypes = {
  onClickOutSide: PropTypes.func.isRequired,
}

export default OutsideDetection
