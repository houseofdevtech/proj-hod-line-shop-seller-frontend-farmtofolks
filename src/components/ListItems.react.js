import React, { useState } from 'react'
import ContentEditable from 'react-contenteditable'
import _ from 'lodash'
import { Segment, Button } from 'semantic-ui-react'

import OutSideDetection from './OutsideDetection.react'
import SelectableCardList from './SelectableCardList.react'

import './ListItems.css'

const ListItems = (props) => {
  const [enableRemoveItem, setEnableRemoveItem] = useState(false)
  const [draftItems, setDraftItem] = useState([])

  const toggleRemoveItem = () => {
    if (props.disabled) {
      return
    }
    setEnableRemoveItem(!enableRemoveItem)
  }
  const onAddNewDraft = () => {
    setDraftItem([...draftItems, 'ลักษณะ'])
  }

  const onResetContent = (index) => {
    const copy = draftItems.slice(0)
    copy[index] = null
    setDraftItem(copy)
  }

  const onSubItemValueChange = (index) => (e) => {
    const copy = draftItems.slice(0)
    copy[index] = e.target.value
    setDraftItem(copy)
  }

  const onDraftItemSave = () => {
    const currentItems = props.items.map((item) => item.value)
    const isDuplicateItem = _.includes(
      _.map(draftItems, (item) => _.includes(currentItems, item)),
      true,
    )

    if (_.isEmpty(_.compact(draftItems)) || isDuplicateItem) {
      return
    }

    props.onDraftItemSave(props.title, draftItems)
    setDraftItem([])
  }

  const renderDraftITem = () => {
    return draftItems.map((item, index) => (
      <ContentEditable
        key={index}
        html={item}
        className="list-items__items__draft"
        onClick={() => onResetContent(index)}
        onChange={onSubItemValueChange(index)}
      />
    ))
  }

  const renderLists = () => (
    <SelectableCardList
      disabled={props.disabled}
      contents={props.items}
      enableRemoveItem={enableRemoveItem}
      onItemDelete={props.onItemDelete}
      maxSelectable={100}
      currentSelected={props.currentSelected}
      onGetSelectedItems={props.onGetSelectedItems}
      multiple
    />
  )

  return (
    <div className="list-items">
      <OutSideDetection onClickOutSide={onDraftItemSave}>
        <Segment>
          <div className="list-items__title-edit">
            <p>{props.title}</p>
            <span className="list-items__edit-button" onClick={toggleRemoveItem}>
              {enableRemoveItem ? (
                <span className="list_items__left-top  list-items__save">บันทึก</span>
              ) : (
                <span className="list_items__left-top ">แก้ไข</span>
              )}
            </span>
          </div>
          <div className="list-items__items">
            {renderLists()}
            {renderDraftITem()}
            {!props.disabled && (
              <Button className="list-items__button" onClick={onAddNewDraft}>
                <span>+</span>
                <span>เพิ่ม</span>
              </Button>
            )}
          </div>
        </Segment>
      </OutSideDetection>
    </div>
  )
}

export default ListItems
