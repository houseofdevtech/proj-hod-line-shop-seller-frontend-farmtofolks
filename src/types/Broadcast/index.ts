export type Broadcast = {
  broadcast_id: string
  broadcast_code: string
  broadcast_type: string
  main_img_path: string | null
  broadcast_items: {
    broadcast_image_map_id: number
    product_group_id: number
  }[]
}

export type BroadcastShop = {
  shop_id: number
  broadcast_data: Broadcast[]
}
