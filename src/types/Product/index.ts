export type Product = {
  shop_id: number
  shop_name: string
  product_data: ProductItem[]
}

export type ProductItem = {
  sku_id: number
  sku_code: string
  color: {
    color_name: string
    color_value: string
    color_img_path: string
  }
  size: string
  other: ProductOtherItem[]
  main_img_path: string
  sub_img_path: string[]
  price: number
  qty: number
  status: number
}

export type ProductDataItem = {
  sku_code: string
  qty: number
  price: number
  color: string
  color_value: string
  color_img_path: string
  size: string
}

export type ProductOtherItem = { name: string; value: string }
