import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import Liff from './src/Liff'
import store from './src/store'

const Root = () => (
  <Provider store={store}>
    <Liff />
  </Provider>
)

ReactDOM.render(<Root />, document.getElementById('root'))
